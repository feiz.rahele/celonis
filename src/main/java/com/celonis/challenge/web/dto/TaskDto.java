package com.celonis.challenge.web.dto;

import java.util.Date;

public class TaskDto {

    /**
     * Id of an entity
     */
    private String id;

    /**
     * name of the task
     */
    private String name;

    /**
     * creation date of task
     */
    private Date creationDate;

    /**
     * task status
     */
    private String taskStatus;

    /**
     * task type
     */
    private String taskType;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

}
