package com.celonis.challenge.web.dto;

public class CounterTaskDto extends TaskDto {

    /**
     * x in counter task  starts from x during execution of task
     */
    private Integer x;

    /**
     * y in counter task finished in y
     */
    private Integer y;

    /**
     * counter in counter task shows execution
     */
    private Integer counter;

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }
}
