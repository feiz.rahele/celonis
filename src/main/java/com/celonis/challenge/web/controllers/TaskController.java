package com.celonis.challenge.web.controllers;

import com.celonis.challenge.exceptions.NotBeConfiguredException;
import com.celonis.challenge.exceptions.NotExecutedTask;
import com.celonis.challenge.model.CounterTask;
import com.celonis.challenge.model.ProjectGenerationTask;
import com.celonis.challenge.model.Task;
import com.celonis.challenge.model.TaskType;
import com.celonis.challenge.services.CounterTaskService;
import com.celonis.challenge.services.FileService;
import com.celonis.challenge.services.ProjectTaskService;
import com.celonis.challenge.services.TaskService;
import com.celonis.challenge.web.converter.TaskConverter;
import com.celonis.challenge.web.dto.CounterTaskDto;
import com.celonis.challenge.web.dto.TaskDto;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * * REST controller for {@link Task}.
 * *
 * * @author Raheleh
 */
@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    /**
     * Access task service
     */
    private final TaskService taskService;

    /**
     * Access file service
     */
    private final FileService fileService;

    /**
     * Access counterTask service
     */
    private final CounterTaskService counterTaskService;

    /**
     * Access projectTask service
     */
    private final ProjectTaskService projectTaskService;
    /**
     * Access task converter
     */
    private final TaskConverter taskConverter;

    /**
     * @param taskService
     * @param fileService
     * @param counterTaskService
     * @param projectTaskService
     * @param taskConverter
     */
    public TaskController(TaskService taskService,
                          FileService fileService, CounterTaskService counterTaskService, ProjectTaskService projectTaskService, TaskConverter taskConverter) {
        this.taskService = taskService;
        this.fileService = fileService;
        this.counterTaskService = counterTaskService;
        this.projectTaskService = projectTaskService;
        this.taskConverter = taskConverter;
    }

    /**
     * @return Retrieved collection of {@code TaskDto child}
     */
    @GetMapping("/")
    public ResponseEntity listTasks() {
        List<Task> tasks = taskService.listTasks();
        ArrayList<Object> list = new ArrayList<>();
        for (Task task : tasks) {
            if (task.getTaskType().equals(TaskType.COUNTER_TYPE)) {
                list.add(taskConverter.convertCounterTaskToCounterTaskDto((CounterTask) task));
            } else {
                list.add(taskConverter.convertProjectTaskToProjectTaskDto((ProjectGenerationTask) task));
            }
        }
        return ResponseEntity.ok(list);
    }

    /**
     * Create a new {@code Task}.
     *
     * @param taskDto Submitted {@code Task} DTO
     * @return Appropriate response
     */
    @PostMapping("/")
    public ResponseEntity createTask(@RequestBody @Valid TaskDto taskDto) {
        TaskDto savedTaskDto;
        if (taskDto.getTaskType().equals(TaskType.COUNTER_TYPE)) {
            CounterTask counterTask = new CounterTask();
            counterTask = taskConverter.convertTaskDtoToTask(taskDto, counterTask);
            savedTaskDto = taskConverter.convertTaskToTaskDto(counterTaskService.createTask(counterTask));
        } else {
            ProjectGenerationTask projectGenerationTask = new ProjectGenerationTask();
            projectGenerationTask = taskConverter.convertTaskDtoToTask(taskDto, projectGenerationTask);
            savedTaskDto = taskConverter.convertTaskToTaskDto(projectTaskService.createTask(projectGenerationTask));
        }
        return new ResponseEntity(savedTaskDto, HttpStatus.CREATED);

    }

    /**
     * @param taskId
     * @return Retrieved  {@code TaskDto child}
     */
    @GetMapping("/{taskId}")
    public ResponseEntity getTask(@PathVariable String taskId) {
        Task task = taskService.getTask(taskId);

        if (task.getTaskType().equals(TaskType.COUNTER_TYPE)) {
            return ResponseEntity.ok(taskConverter.convertCounterTaskToCounterTaskDto((CounterTask) task));
        } else {
            return ResponseEntity.ok(taskConverter.convertProjectTaskToProjectTaskDto((ProjectGenerationTask) task));
        }

    }

    /**
     * Update a persisted {@code Task}.
     *
     * @param taskDto DTO populated with current entity data
     * @return Appropriate response
     */
    @PutMapping("/{taskId}")
    public ResponseEntity updateTask(@PathVariable String taskId,
                                     @RequestBody @Valid TaskDto taskDto) {

        TaskDto updateTaskDto;
        if (taskDto.getTaskType().equals(TaskType.COUNTER_TYPE)) {
            CounterTask counterTask = new CounterTask();
            counterTask = taskConverter.convertTaskDtoToTask(taskDto, counterTask);
            updateTaskDto = taskConverter.convertTaskToTaskDto(counterTaskService.update(taskId, counterTask));
        } else {
            ProjectGenerationTask projectGenerationTask = new ProjectGenerationTask();
            projectGenerationTask = taskConverter.convertTaskDtoToTask(taskDto, projectGenerationTask);

            updateTaskDto = taskConverter.convertTaskToTaskDto(projectTaskService.update(taskId, projectGenerationTask));
        }
        return ResponseEntity.ok(updateTaskDto);

    }

    /**
     * Delete a {@code Task}.
     *
     * @param taskId Id of the requested entity
     */
    @DeleteMapping("/{taskId}")
    public ResponseEntity deleteTask(@PathVariable String taskId) {
        taskService.delete(taskId);
        return ResponseEntity.noContent().build();
    }

    /**
     * Execute a {@code Task}.
     *
     * @param taskId Id of the requested entity
     * @throws NotBeConfiguredException X or Y are null
     */
    @PostMapping("/{taskId}/execute")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity executeTask(@PathVariable String taskId) throws NotBeConfiguredException
    {
        Task task = taskService.getTask(taskId);

        if (task.getTaskType().equals(TaskType.COUNTER_TYPE)) {
            counterTaskService.executeTask((CounterTask) task);
        } else {
            projectTaskService.executeTask(taskId);
        }
        return ResponseEntity.noContent().build();

    }

    /**
     * Configure a {@code CounterTask}.
     *
     * @param taskId Id of the requested entity
     * @return configured counter task
     * @throws NotBeConfiguredException X or Y are null
     */
    @PostMapping("/{taskId}/configure")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity configureTaskCounter(@PathVariable String taskId, @RequestBody @Valid CounterTask counterTask) throws NotBeConfiguredException {

        CounterTaskDto counterTaskDto = taskConverter.convertCounterTaskToCounterTaskDto(counterTaskService.configureCounterTask(taskId, counterTask));
        return ResponseEntity.ok(counterTaskDto);
    }

    /**
     * Monitor a {@code CounterTask}.
     *
     * @param taskId Id of the requested entity
     * @return monitor counter task
     */
    @GetMapping("/{taskId}/monitor")
    public ResponseEntity monitorTask(@PathVariable String taskId) {
        TaskDto taskDto;
        taskDto = taskConverter.convertTaskToTaskDto(counterTaskService.monitorTask(taskId));
        return ResponseEntity.ok(taskDto);
    }

    @GetMapping("/{taskId}/result")
    public ResponseEntity<FileSystemResource> getResult(@PathVariable String taskId) {
        ProjectGenerationTask projectGenerationTask = taskService.getTask(taskId);
        return fileService.getTaskResult(projectGenerationTask);
    }

    /**
     * Pause a {@code CounterTask}.
     *
     * @param taskId Id of the requested entity
     * @return paused counter task
     * @throws NotExecutedTask task is not executed to be canceled
     */
    @PostMapping("/{taskId}/cancel")
    public <T extends TaskDto> ResponseEntity cancelTaskExecution(@PathVariable String taskId, @RequestBody T taskDto) throws NotExecutedTask {
        CounterTask counterTask = new CounterTask();
        if (taskDto.getTaskType().equals(TaskType.COUNTER_TYPE)) {
            counterTask = counterTaskService.cancel(taskId);
        } else {
            projectTaskService.cancel(taskId);
        }
        return ResponseEntity.ok(counterTask);

    }


}