package com.celonis.challenge.web.converter;

import com.celonis.challenge.model.CounterTask;
import com.celonis.challenge.model.ProjectGenerationTask;
import com.celonis.challenge.model.Task;
import com.celonis.challenge.web.dto.CounterTaskDto;
import com.celonis.challenge.web.dto.ProjectTaskDto;
import com.celonis.challenge.web.dto.TaskDto;
import org.springframework.stereotype.Component;

@Component
public class TaskConverter {

/**
 * convert task dto to task
 *
 */

    public <T, G extends Task> T convertTaskDtoToTask(TaskDto taskDto, G task) {
        task.setName(taskDto.getName());
        task.setTaskStatus(taskDto.getTaskStatus());
        task.setTaskType(taskDto.getTaskType());
        return (T) task;
    }

    public <T extends TaskDto, G extends Task> T convertTaskToTaskDto(G task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setName(task.getName());
        taskDto.setTaskStatus(task.getTaskStatus());
        taskDto.setTaskType(task.getTaskType());
        return (T) taskDto;
    }

    public CounterTask convertCounterTaskDtoToCounterTask(CounterTaskDto taskDto) {
        CounterTask counterTask=new CounterTask();
        counterTask.setName(taskDto.getName());
        counterTask.setTaskStatus(taskDto.getTaskStatus());
        counterTask.setTaskType(taskDto.getTaskType());
        counterTask.setX(taskDto.getX());
        counterTask.setY(taskDto.getY());
        return counterTask;
    }

    public CounterTaskDto convertCounterTaskToCounterTaskDto(CounterTask task) {
        CounterTaskDto counterTaskDto = new CounterTaskDto();
        counterTaskDto.setId(task.getId());
        counterTaskDto.setName(task.getName());
        counterTaskDto.setCreationDate(task.getCreationDate());
        counterTaskDto.setTaskStatus(task.getTaskStatus());
        counterTaskDto.setTaskType(task.getTaskType());
        counterTaskDto.setX(task.getX());
        counterTaskDto.setY(task.getY());
        return counterTaskDto;
    }

    public ProjectTaskDto convertProjectTaskToProjectTaskDto(ProjectGenerationTask task) {
        ProjectTaskDto projectTaskDto = new ProjectTaskDto();
        projectTaskDto.setName(task.getName());
        projectTaskDto.setId(task.getId());
        projectTaskDto.setTaskStatus(task.getTaskStatus());
        projectTaskDto.setTaskType(task.getTaskType());
        projectTaskDto.setCreationDate(task.getCreationDate());
        return projectTaskDto;
    }

    public ProjectGenerationTask convertProjectTaskDtoToProjectTask(ProjectTaskDto taskDto) {
        ProjectGenerationTask projectGenerationTask = new ProjectGenerationTask();
        projectGenerationTask.setName(taskDto.getName());
        projectGenerationTask.setTaskStatus(taskDto.getTaskStatus());
        projectGenerationTask.setTaskType(taskDto.getTaskType());
        projectGenerationTask.setName(taskDto.getName());
        projectGenerationTask.setTaskStatus(taskDto.getTaskStatus());
        projectGenerationTask.setTaskType(taskDto.getTaskType());
        projectGenerationTask.setCreationDate(taskDto.getCreationDate());
        return projectGenerationTask;
    }
}
