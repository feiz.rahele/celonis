package com.celonis.challenge.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(TaskType.COUNTER_TYPE)
public class CounterTask extends Task {
    /**
     * x in counter task  starts from x during execution of task
     */
    @Column(name = "x")
    private Integer x;

    /**
     * y in counter task finished in y
     */
    @Column(name = "y")
    private Integer y;

    /**
     * counter in counter task shows execution
     */
    @Column(name = "counter")
    private Integer counter;

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getCounter() {
        return counter;
    }

    public void setCounter(Integer counter) {
        this.counter = counter;
    }
}
