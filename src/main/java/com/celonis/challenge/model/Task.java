package com.celonis.challenge.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity
@DiscriminatorColumn(name = "TASK_TYPE")
public class Task {

    /**
     * Id of an entity
     */
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    /**
     * name of the task
     */
    @NotNull(message = "{task.name.null}")
    @Column(name = "name", nullable = false)
    private String name;

    /**
     * creation date of task
     */
    @Column(name = "creation_date")
    private Date creationDate;

    /**
     * task status
     */
    @Column(name = "task_status")

    private String taskStatus;

    /**
     * task type
     */

    @Column(name = "TASK_TYPE", insertable = false, updatable = false)
    protected String taskType;

    @Version
    @Column(name = "VERSION")
    private long version;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }
}
