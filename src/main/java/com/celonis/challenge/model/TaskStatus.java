package com.celonis.challenge.model;

public interface TaskStatus {
    public static final String FINISHED_STATUS = "FINISHED";
    public static final String EXECUTED_STATUS= "EXECUTED";
    public static final String CANCELED_STATUS= "CANCELED";
    public static final String CREATED_STATUS= "CREATED";

}
