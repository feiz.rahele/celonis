package com.celonis.challenge.model;

    public interface TaskType{
        public static final String PROJECT_TYPE = "PROJECT";
        public static final String COUNTER_TYPE = "COUNTER";
    }

