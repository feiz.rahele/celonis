package com.celonis.challenge.services;

import com.celonis.challenge.model.Task;
import com.celonis.challenge.model.TaskStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * In this class is related to scheduled tasks
 */

@Component
public class ScheduledTasks {

    private final TaskService taskService;

    public ScheduledTasks(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Removed the tasks which after a week has not executed yet.
     */
    @Scheduled(fixedRate = 2 * 60 * 1000)
    public void removeNotExecutedTaskPerWeek() {
        List<Task> tasks = taskService.listTasks();
        List<Task> notExecutedTasks = tasks.stream().filter(task -> differBetweenTwoDate(new Date(), task.getCreationDate()) > 1
                && task.getTaskStatus().equals(TaskStatus.CREATED_STATUS)).collect(Collectors.toList());
        if (notExecutedTasks.size() != 0) {
            notExecutedTasks.forEach(task -> taskService.delete(task.getId()));
        }
    }

    /**
     * Find diference between two dates in terms of day
     */
    private long differBetweenTwoDate(Date now, Date createdTaskDate) {
        long diffInMillies = Math.abs(now.getTime() - createdTaskDate.getTime());
        return TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);

    }
}
