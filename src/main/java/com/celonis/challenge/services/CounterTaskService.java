package com.celonis.challenge.services;

import com.celonis.challenge.exceptions.NotBeConfigurableException;
import com.celonis.challenge.exceptions.NotBeConfiguredException;
import com.celonis.challenge.exceptions.NotExecutedTask;
import com.celonis.challenge.model.*;
import com.celonis.challenge.repository.CounterTaskRepository;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class CounterTaskService {

    private final CounterTaskRepository counterTaskRepository;
    private final TaskService taskService;

    private Boolean isExecutedTask = true;
    private Boolean isCancelStatus = false;


    public CounterTaskService(CounterTaskRepository counterTaskRepository, TaskService taskService) {
        this.counterTaskRepository = counterTaskRepository;
        this.taskService = taskService;
    }

    /**
     * Configure counter task by setting value of x , y
     *
     * @param taskId      id of task
     * @param counterTask as configured task
     * @return saved configured task
     */

    public CounterTask configureCounterTask(String taskId, CounterTask counterTask) throws NotBeConfiguredException {
        CounterTask task = taskService.getTask(taskId);
        if (task.getTaskType().equals(TaskType.PROJECT_TYPE)) {
            throw new NotBeConfigurableException("ThisTypeProjectIsNotConfigurable");
        }
        if(counterTask.getX()==null || counterTask.getY()==null){
            throw new NotBeConfiguredException("TaskShouldBeConfigured");
        }
        counterTask.setCreationDate(task.getCreationDate());
        counterTask.setName(task.getName());
        counterTask.setId(task.getId());
        counterTask.setTaskType(task.getTaskType());
        counterTask.setTaskStatus(task.getTaskStatus());
        return counterTaskRepository.save(counterTask);
    }

    /**
     * create counter task
     *
     * @param counterTask
     * @return created counter task
     */

    public CounterTask createTask(CounterTask counterTask) {
        counterTask.setId(null);
        counterTask.setCreationDate(new Date());
        counterTask.setTaskStatus(TaskStatus.CREATED_STATUS);
//        counterTask.setTaskType(TaskType.COUNTER_TYPE);
        return counterTaskRepository.save(counterTask);
    }

    /**
     * execute counter task, task runs in background
     *
     * @param task
     */

    public void executeTask(CounterTask task) throws NotBeConfiguredException {
        if(task.getX()==null || task.getY()==null){
            throw new NotBeConfiguredException("TaskShouldBeConfigured");
        }
        AtomicInteger count = new AtomicInteger();
        int counter = task.getY() - task.getX();
        task.setTaskStatus(TaskStatus.EXECUTED_STATUS);
        counterTaskRepository.save(task);
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        Runnable counterTask = () -> {
            count.getAndIncrement();
        };
        ScheduledFuture<?> scheduledFuture = ses.scheduleAtFixedRate(counterTask, 0, 1, TimeUnit.SECONDS);
        while (true) {
            task.setCounter(count.get() + task.getX());
            counterTaskRepository.save(task);
            if (count.get() == counter) {
                scheduledFuture.cancel(true);
                ses.shutdown();
                task.setTaskStatus(TaskStatus.FINISHED_STATUS);
                counterTaskRepository.save(task);
                break;
            }
            CounterTask updatedTask = taskService.getTask(task.getId());
            if (updatedTask.equals(Constants.TaskStatus.CANCELED_STATUS)) {
                scheduledFuture.cancel(true);
                ses.shutdown();
                task.setTaskStatus(TaskStatus.CANCELED_STATUS);
                counterTaskRepository.save(task);
                isCancelStatus = true;
                break;
            }
        }
        isExecutedTask = true;
    }

    /**
     * monitor task by getting updated value of counter in task
     *
     * @param taskId id of task
     * @return counter task
     */

    public CounterTask monitorTask(String taskId) {
        return taskService.getTask(taskId);
    }

    /**
     * update the value of counter task
     *
     * @param taskId      id of task
     * @param counterTask
     * @return updated counter task
     */

    public CounterTask update(String taskId, CounterTask counterTask) {
        CounterTask existing = taskService.get(taskId);
        existing.setName(counterTask.getName());
        existing.setCreationDate(counterTask.getCreationDate());
        return counterTaskRepository.save(existing);
    }

    /**
     * pause executed counter task
     *
     * @param taskId
     * @return paused task
     */

    public CounterTask cancel(String taskId) throws NotExecutedTask {
        CounterTask task = taskService.getTask(taskId);
        task.setTaskStatus(Constants.TaskStatus.CANCELED_STATUS);
        counterTaskRepository.save(task);
    }
}