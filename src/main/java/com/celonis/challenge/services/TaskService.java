package com.celonis.challenge.services;

import com.celonis.challenge.exceptions.NotFoundException;
import com.celonis.challenge.model.ProjectGenerationTask;
import com.celonis.challenge.model.Task;
import com.celonis.challenge.repository.TaskRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Find all task
     *
     */
    public List<Task> listTasks() {
        return taskRepository.findAll();
    }

    /**
     * find a task with id.
     *
     */
    public <T extends Task> T get(String taskId) {
        T task = (T) taskRepository.findById(taskId);
        if (task == null) {
            throw new NotFoundException();
        }
        return task;
    }

    /**
     * Get a task with id.
     *
     */
    public <T extends Task> T getTask(String taskId) {
        return get(taskId);
    }

    /**
     * Delete a task with id.
     *
     */
    public void delete(String taskId) {
        taskRepository.delete(taskId);
    }


}
