package com.celonis.challenge.services;

import com.celonis.challenge.exceptions.InternalException;
import com.celonis.challenge.exceptions.NotFoundException;
import com.celonis.challenge.model.ProjectGenerationTask;
import com.celonis.challenge.model.TaskStatus;
import com.celonis.challenge.model.TaskType;
import com.celonis.challenge.repository.ProjectGenerationTaskRepository;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.Date;

@Service
public class ProjectTaskService {

    private final ProjectGenerationTaskRepository projectGenerationTaskRepository;

    private final TaskService taskService;

    private final FileService fileService;


    public ProjectTaskService(ProjectGenerationTaskRepository projectGenerationTaskRepository,
                              TaskService taskService, FileService fileService) {
        this.projectGenerationTaskRepository = projectGenerationTaskRepository;
        this.taskService = taskService;
        this.fileService = fileService;
    }

    /**
     * create counter task
     *
     * @param projectGenerationTask
     * @return created project task
     */
    public ProjectGenerationTask createTask(ProjectGenerationTask projectGenerationTask) {
        projectGenerationTask.setId(null);
        projectGenerationTask.setCreationDate(new Date());
        projectGenerationTask.setTaskStatus(TaskStatus.CREATED_STATUS);
        projectGenerationTask.setTaskType(TaskType.PROJECT_TYPE);
        return projectGenerationTaskRepository.save(projectGenerationTask);
    }



    public ProjectGenerationTask update(String taskId, ProjectGenerationTask projectGenerationTask) {
        ProjectGenerationTask existing = taskService.get(taskId);
        existing.setCreationDate(projectGenerationTask.getCreationDate());
        existing.setName(projectGenerationTask.getName());
        return projectGenerationTaskRepository.save(existing);
    }

    /**
     * execute task
     *
     * @param taskId id of task
     */
    public void executeTask(String taskId) {
        URL url = Thread.currentThread().getContextClassLoader().getResource("challenge.zip");
        if (url == null) {
            throw new InternalException("Zip file not found");
        }
        try {
            ProjectGenerationTask projectGenerationTask = taskService.getTask(taskId);
            fileService.storeResult(taskId, url, projectGenerationTask);
        } catch (Exception e) {
            throw new InternalException(e);
        }
    }


    /**
     * cancel executed task
     *
     * @param taskId
     * @return canceled task
     */

    public ProjectGenerationTask cancel(String taskId) {
        ProjectGenerationTask projectGenerationTask = projectGenerationTaskRepository.findById(taskId);
        projectGenerationTask.setTaskStatus(TaskStatus.CANCELED_STATUS);
        return projectGenerationTaskRepository.save(projectGenerationTask);
    }
}
