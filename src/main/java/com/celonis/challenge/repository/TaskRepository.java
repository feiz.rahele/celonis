package com.celonis.challenge.repository;

import com.celonis.challenge.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface TaskRepository extends TaskBaseRepository<Task>  {
}
