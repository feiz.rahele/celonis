package com.celonis.challenge.repository;

import com.celonis.challenge.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface TaskBaseRepository<T extends Task>  extends JpaRepository<T, String> {
    /**
     * Find a {@code Task} by its id.
     *
     * @param taskId id
     * @return {@code Task}
     */
    public T findById(String taskId);

    /**
     * Find all tasks {@code Task}
     * @return list of task {@code Task}
     */

    List<T> findAll();
}