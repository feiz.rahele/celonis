package com.celonis.challenge.repository;

import com.celonis.challenge.model.ProjectGenerationTask;

import javax.transaction.Transactional;


@Transactional
public interface ProjectGenerationTaskRepository extends TaskBaseRepository<ProjectGenerationTask> {
}
