package com.celonis.challenge.repository;

import com.celonis.challenge.model.CounterTask;

import javax.transaction.Transactional;

@Transactional
public interface CounterTaskRepository extends TaskBaseRepository<CounterTask> {
}
