package com.celonis.challenge.exceptions;

import org.hibernate.service.spi.ServiceException;

public class NotExecutedTask extends ServiceException {
    public NotExecutedTask(String message){
        super(message);
    }
}
