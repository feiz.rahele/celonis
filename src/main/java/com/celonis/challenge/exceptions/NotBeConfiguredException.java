package com.celonis.challenge.exceptions;

import org.hibernate.service.spi.ServiceException;

public class NotBeConfiguredException extends ServiceException {
    public NotBeConfiguredException(String message) {
        super(message);
    }
}
