package com.celonis.challenge.exceptions;

import org.hibernate.service.spi.ServiceException;

public class NotBeConfigurableException extends ServiceException {
    public NotBeConfigurableException(String message){
        super(message);
    }
}
